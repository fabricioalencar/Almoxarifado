<%@ page import="almoxarifado.Locador" %>



<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="locador.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" maxlength="35" required="" value="${locadorInstance?.nome}"placeholder="Informe seu nome"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'numero', 'error')} required">
	<label for="numero">
		<g:message code="locador.numero.label" default="Numero" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="numero" maxlength="35" required="" value="${locadorInstance?.numero}"placeholder="Informe o numero"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="locador.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${locadorInstance?.email}"placeholder="informe o email"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'sexo', 'error')} required">
	<label for="sexo">
		<g:message code="locador.sexo.label" default="Sexo" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="sexo" from="${locadorInstance.constraints.sexo.inList}" required="" value="${locadorInstance?.sexo}" valueMessagePrefix="locador.sexo"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'bairro', 'error')} required">
	<label for="bairro">
		<g:message code="locador.bairro.label" default="Bairro" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="bairro" required="" value="${locadorInstance?.bairro}"placeholder="Informe o bairro"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'rua', 'error')} required">
	<label for="rua">
		<g:message code="locador.rua.label" default="Rua" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="rua" required="" value="${locadorInstance?.rua}"placeholder="Informe a rua"/>

</div>

<div class="fieldcontain ${hasErrors(bean: locadorInstance, field: 'telefone', 'error')} required">
	<label for="telefone">
		<g:message code="locador.telefone.label" default="Telefone" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="telefone" required="" value="${locadorInstance?.telefone}"placeholder="solicite seu telefone"/>

</div>

