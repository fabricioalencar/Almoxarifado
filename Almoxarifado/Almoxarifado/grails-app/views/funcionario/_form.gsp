<%@ page import="almoxarifado.Funcionario" %>



<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="funcionario.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${funcionarioInstance?.username}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="funcionario.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${funcionarioInstance?.password}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'tipo', 'error')} required">
	<label for="tipo">
		<g:message code="funcionario.tipo.label" default="Tipo" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="tipo" from="${funcionarioInstance.constraints.tipo.inList}" required="" value="${funcionarioInstance?.tipo}" valueMessagePrefix="funcionario.tipo"/>

</div>

<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="funcionario.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${funcionarioInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="funcionario.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${funcionarioInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'accountExpired', 'error')} ">
	<label for="accountExpired">
		<g:message code="funcionario.accountExpired.label" default="Account Expired" />
		
	</label>
	<g:checkBox name="accountExpired" value="${funcionarioInstance?.accountExpired}" />

</div>

<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'accountLocked', 'error')} ">
	<label for="accountLocked">
		<g:message code="funcionario.accountLocked.label" default="Account Locked" />
		
	</label>
	<g:checkBox name="accountLocked" value="${funcionarioInstance?.accountLocked}" />

</div>

<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'enabled', 'error')} ">
	<label for="enabled">
		<g:message code="funcionario.enabled.label" default="Enabled" />
		
	</label>
	<g:checkBox name="enabled" value="${funcionarioInstance?.enabled}" />

</div>

<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'passwordExpired', 'error')} ">
	<label for="passwordExpired">
		<g:message code="funcionario.passwordExpired.label" default="Password Expired" />
		
	</label>
	<g:checkBox name="passwordExpired" value="${funcionarioInstance?.passwordExpired}" />

</div>

<div class="fieldcontain ${hasErrors(bean: funcionarioInstance, field: 'telefone', 'error')} required">
	<label for="telefone">
		<g:message code="funcionario.telefone.label" default="Telefone" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="telefone" required="" value="${funcionarioInstance?.telefone}"/>

</div>

