
<%@ page import="almoxarifado.RegistroBC" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'registroBC.label', default: 'RegistroBC')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-registroBC" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-registroBC" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list registroBC">
			
				<g:if test="${registroBCInstance?.alocacao}">
				<li class="fieldcontain">
					<span id="alocacao-label" class="property-label"><g:message code="registroBC.alocacao.label" default="Alocacao" /></span>
					
						<span class="property-value" aria-labelledby="alocacao-label"><g:link controller="alocacao" action="show" id="${registroBCInstance?.alocacao?.id}">${registroBCInstance?.alocacao?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${registroBCInstance?.bensdeconsumo}">
				<li class="fieldcontain">
					<span id="bensdeconsumo-label" class="property-label"><g:message code="registroBC.bensdeconsumo.label" default="Bensdeconsumo" /></span>
					
						<span class="property-value" aria-labelledby="bensdeconsumo-label"><g:link controller="bensDeConsumo" action="show" id="${registroBCInstance?.bensdeconsumo?.id}">${registroBCInstance?.bensdeconsumo?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${registroBCInstance?.funcionario}">
				<li class="fieldcontain">
					<span id="funcionario-label" class="property-label"><g:message code="registroBC.funcionario.label" default="Funcionario" /></span>
					
						<span class="property-value" aria-labelledby="funcionario-label"><g:link controller="funcionario" action="show" id="${registroBCInstance?.funcionario?.id}">${registroBCInstance?.funcionario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${registroBCInstance?.locador}">
				<li class="fieldcontain">
					<span id="locador-label" class="property-label"><g:message code="registroBC.locador.label" default="Locador" /></span>
					
						<span class="property-value" aria-labelledby="locador-label"><g:link controller="locador" action="show" id="${registroBCInstance?.locador?.id}">${registroBCInstance?.locador?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${registroBCInstance?.relatorio}">
				<li class="fieldcontain">
					<span id="relatorio-label" class="property-label"><g:message code="registroBC.relatorio.label" default="Relatorio" /></span>
					
						<span class="property-value" aria-labelledby="relatorio-label"><g:link controller="relatorio" action="show" id="${registroBCInstance?.relatorio?.id}">${registroBCInstance?.relatorio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:registroBCInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${registroBCInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
