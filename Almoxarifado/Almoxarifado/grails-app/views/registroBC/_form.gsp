<%@ page import="almoxarifado.RegistroBC" %>



<div class="fieldcontain ${hasErrors(bean: registroBCInstance, field: 'alocacao', 'error')} required">
	<label for="alocacao">
		<g:message code="registroBC.alocacao.label" default="Alocacao" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="alocacao" name="alocacao.id" from="${almoxarifado.Alocacao.list()}" optionKey="id" required="" value="${registroBCInstance?.alocacao?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: registroBCInstance, field: 'bensdeconsumo', 'error')} required">
	<label for="bensdeconsumo">
		<g:message code="registroBC.bensdeconsumo.label" default="Bensdeconsumo" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="bensdeconsumo" name="bensdeconsumo.id" from="${almoxarifado.BensDeConsumo.list()}" optionKey="id" required="" value="${registroBCInstance?.bensdeconsumo?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: registroBCInstance, field: 'funcionario', 'error')} required">
	<label for="funcionario">
		<g:message code="registroBC.funcionario.label" default="Funcionario" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="funcionario" name="funcionario.id" from="${almoxarifado.Funcionario.list()}" optionKey="id" required="" value="${registroBCInstance?.funcionario?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: registroBCInstance, field: 'locador', 'error')} required">
	<label for="locador">
		<g:message code="registroBC.locador.label" default="Locador" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="locador" name="locador.id" from="${almoxarifado.Locador.list()}" optionKey="id" required="" value="${registroBCInstance?.locador?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: registroBCInstance, field: 'relatorio', 'error')} required">
	<label for="relatorio">
		<g:message code="registroBC.relatorio.label" default="Relatorio" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="relatorio" name="relatorio.id" from="${almoxarifado.Relatorio.list()}" optionKey="id" required="" value="${registroBCInstance?.relatorio?.id}" class="many-to-one"/>

</div>

