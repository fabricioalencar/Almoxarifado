<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/>Almoxarifado</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">	
		<link rel="icon" sizes="114x114" href="${assetPath(src: 'icon.png')}">
                <script type="text/javascript" src="${resource(dir:'javascripts',file:'application.js')}"></script>
                <asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
               <g:layoutHead/>
        </head>        
  

  <body>      
			<div id="nav">
                                                            				
                             <sec:ifLoggedIn>
                                  
                <div id="main">
                    <header>
                        <div id="logo">
                            <div id="logo_text">
                                <!-- class="logo_colour", allows you to change the colour of the text -->
                                <h1><a href="index.html">Almo<span class="logo_colour">xarifado</span></a></h1>
                   
                           <nav>
                                <ul class="sf-menu" id="nav">
                                   <li><g:link controller="relatorio" action="index" title="Vizualizar Relatorio">Relatório</g:link></li>
                                   <li><g:link controller="funcionario" action="index" title="Gerenciar Funcionario"> Funcionário</g:link></li>
                                   <li><g:link controller="locador" action="index" title="Gerenciar Locador"> Locador</g:link></li>
                                   <li><g:link controller="Permanente" action="index" title="Gerenciar Permanentes">B.Permanente</g:link></li>
                                   <li><g:link controller="BensDeConsumo" action="index" title="Gerenciar Bens de Consumo">B.Consumo</g:link></li>
                                   <li><g:link controller="alocacao" action="index" title="Gerenciar Alocação"> Alocacao</g:link></li>
                                </ul>
                            </nav>
                             </div>
                            </div>
                         </header>
            </sec:ifLoggedIn>
            <g:layoutBody/>
                                                   
    
 
  </div>
  </body>
     
</html>
