<!DOCTYPE html>
<html>
	<head>
                <meta name="layout" content="main"/>
		
		<style type="text/css" media="screen">
                <meta name="description" content="website description" />
                <meta name="keywords" content="website keywords, website keywords" />
                <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
                <!--<link rel="stylesheet" type="text/css" href="css/style.css" />-->
                <link rel="stylesheet" href="${resource(dir: 'css', file: 'style.css')}" type="text/css">
                <!-- modernizr enables HTML5 elements and feature detects -->
                <!-- <script type="text/javascript" src="js/modernizr-1.5.min.js"></script>-->  
                 <script type="text/javascript" src="${resource(dir:'js',file:'modernizr-1.5.min.js')}"></script>
                    
		</style>
	</head>
<body>
  <div id="main">
   <div id="site_content">
      <ul id="images">
        <li><img src="images/1.jpg" width="600" height="300" alt="gallery_buildings_one" /></li>
        <li><img src="images/2.jpg" width="600" height="300" alt="gallery_buildings_two" /></li>
        <li><img src="images/3.jpg" width="600" height="300" alt="gallery_buildings_three" /></li>
        <li><img src="images/4.jpg" width="600" height="300" alt="gallery_buildings_four" /></li>
        <li><img src="images/5.jpg" width="600" height="300" alt="gallery_buildings_five" /></li>
        <li><img src="images/6.jpg" width="600" height="300" alt="gallery_buildings_six" /></li>
      </ul>
      
      <div class="content">
        <h1>Aqui você tem um Controle Melhor sobre o Almoxarifado.</h1>
        <p>Informatizar o processo de controle dos dados e atividades desenvolvidas dentro de um almoxarifado através de uma ferramenta web para que possa integrar as atividades desenvolvidas reduzindo custos de tempo de serviços.</p>
        <p>Identificar as necessidades e criar pacotes que auxiliem na elaboração de um modelo de solução para as necessidades do almoxarifado;</p>
      </div>
    </div>
    

  <p>&nbsp;</p>
  <!-- javascript at the bottom for fast page loading -->
  <script type="text/javascript" src="${resource(dir:'js',file:'jquery.js')}"></script>
   <script type="text/javascript" src="${resource(dir:'js',file:'jquery.easing-sooper.js')}"></script>
   <script type="text/javascript" src="${resource(dir:'js',file:'jquery.sooperfish.js')}"></script>
   <script type="text/javascript" src="${resource(dir:'js',file:'jquery.kwicks-1.5.1.js')}"></script>
  <!-- <script type="text/javascript" src="../../jquery.js"></script>
  <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
  <script type="text/javascript" src="js/jquery.kwicks-1.5.1.js"></script>
-->  
 <script type="text/javascript">
  
    $(document).ready(function() {
      $('#images').kwicks({
        max : 600,
        spacing : 2
      });
      $('ul.sf-menu').sooperfish();
    });
  </script>
 </div> 
</body>
</html>
