
<%@ page import="almoxarifado.RegistroBP" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'registroBP.label', default: 'RegistroBP')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-registroBP" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-registroBP" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="registroBP.alocacao.label" default="Alocacao" /></th>
					
						<th><g:message code="registroBP.funcionario.label" default="Funcionario" /></th>
					
						<th><g:message code="registroBP.locador.label" default="Locador" /></th>
					
						<th><g:message code="registroBP.permante.label" default="Permante" /></th>
					
						<th><g:message code="registroBP.relatorio.label" default="Relatorio" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${registroBPInstanceList}" status="i" var="registroBPInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${registroBPInstance.id}">${fieldValue(bean: registroBPInstance, field: "alocacao")}</g:link></td>
					
						<td>${fieldValue(bean: registroBPInstance, field: "funcionario")}</td>
					
						<td>${fieldValue(bean: registroBPInstance, field: "locador")}</td>
					
						<td>${fieldValue(bean: registroBPInstance, field: "permante")}</td>
					
						<td>${fieldValue(bean: registroBPInstance, field: "relatorio")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${registroBPInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
