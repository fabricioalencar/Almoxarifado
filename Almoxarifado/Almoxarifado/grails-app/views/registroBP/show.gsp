
<%@ page import="almoxarifado.RegistroBP" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'registroBP.label', default: 'RegistroBP')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-registroBP" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-registroBP" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list registroBP">
			
				<g:if test="${registroBPInstance?.alocacao}">
				<li class="fieldcontain">
					<span id="alocacao-label" class="property-label"><g:message code="registroBP.alocacao.label" default="Alocacao" /></span>
					
						<span class="property-value" aria-labelledby="alocacao-label"><g:link controller="alocacao" action="show" id="${registroBPInstance?.alocacao?.id}">${registroBPInstance?.alocacao?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${registroBPInstance?.funcionario}">
				<li class="fieldcontain">
					<span id="funcionario-label" class="property-label"><g:message code="registroBP.funcionario.label" default="Funcionario" /></span>
					
						<span class="property-value" aria-labelledby="funcionario-label"><g:link controller="funcionario" action="show" id="${registroBPInstance?.funcionario?.id}">${registroBPInstance?.funcionario?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${registroBPInstance?.locador}">
				<li class="fieldcontain">
					<span id="locador-label" class="property-label"><g:message code="registroBP.locador.label" default="Locador" /></span>
					
						<span class="property-value" aria-labelledby="locador-label"><g:link controller="locador" action="show" id="${registroBPInstance?.locador?.id}">${registroBPInstance?.locador?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${registroBPInstance?.permante}">
				<li class="fieldcontain">
					<span id="permante-label" class="property-label"><g:message code="registroBP.permante.label" default="Permante" /></span>
					
						<span class="property-value" aria-labelledby="permante-label"><g:link controller="permanente" action="show" id="${registroBPInstance?.permante?.id}">${registroBPInstance?.permante?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${registroBPInstance?.relatorio}">
				<li class="fieldcontain">
					<span id="relatorio-label" class="property-label"><g:message code="registroBP.relatorio.label" default="Relatorio" /></span>
					
						<span class="property-value" aria-labelledby="relatorio-label"><g:link controller="relatorio" action="show" id="${registroBPInstance?.relatorio?.id}">${registroBPInstance?.relatorio?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:registroBPInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${registroBPInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
