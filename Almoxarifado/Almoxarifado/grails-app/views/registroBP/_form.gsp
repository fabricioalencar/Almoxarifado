<%@ page import="almoxarifado.RegistroBP" %>



<div class="fieldcontain ${hasErrors(bean: registroBPInstance, field: 'alocacao', 'error')} required">
	<label for="alocacao">
		<g:message code="registroBP.alocacao.label" default="Alocacao" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="alocacao" name="alocacao.id" from="${almoxarifado.Alocacao.list()}" optionKey="id" required="" value="${registroBPInstance?.alocacao?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: registroBPInstance, field: 'funcionario', 'error')} required">
	<label for="funcionario">
		<g:message code="registroBP.funcionario.label" default="Funcionario" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="funcionario" name="funcionario.id" from="${almoxarifado.Funcionario.list()}" optionKey="id" required="" value="${registroBPInstance?.funcionario?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: registroBPInstance, field: 'locador', 'error')} required">
	<label for="locador">
		<g:message code="registroBP.locador.label" default="Locador" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="locador" name="locador.id" from="${almoxarifado.Locador.list()}" optionKey="id" required="" value="${registroBPInstance?.locador?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: registroBPInstance, field: 'permante', 'error')} required">
	<label for="permante">
		<g:message code="registroBP.permante.label" default="Permante" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="permante" name="permante.id" from="${almoxarifado.Permanente.list()}" optionKey="id" required="" value="${registroBPInstance?.permante?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: registroBPInstance, field: 'relatorio', 'error')} required">
	<label for="relatorio">
		<g:message code="registroBP.relatorio.label" default="Relatorio" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="relatorio" name="relatorio.id" from="${almoxarifado.Relatorio.list()}" optionKey="id" required="" value="${registroBPInstance?.relatorio?.id}" class="many-to-one"/>

</div>

