<%@ page import="almoxarifado.Permanente" %>



<div class="fieldcontain ${hasErrors(bean: permanenteInstance, field: 'descricao', 'error')} required">
	<label for="descricao">
		<g:message code="permanente.descricao.label" default="Descricao" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="descricao" required="" value="${permanenteInstance?.descricao}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: permanenteInstance, field: 'quantidade', 'error')} required">
	<label for="quantidade">
		<g:message code="permanente.quantidade.label" default="Quantidade" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="quantidade" type="number" value="${permanenteInstance.quantidade}" required=""/>

</div>

