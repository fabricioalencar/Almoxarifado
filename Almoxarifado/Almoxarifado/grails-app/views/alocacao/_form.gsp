<%@ page import="almoxarifado.Alocacao" %>



<div class="fieldcontain ${hasErrors(bean: alocacaoInstance, field: 'bensdeconsumo', 'error')} ">
	<label for="bensdeconsumo">
		<g:message code="alocacao.bensdeconsumo.label" default="Bensdeconsumo" />
		
	</label>
	<g:select name="bensdeconsumo" from="${almoxarifado.BensDeConsumo.list()}" multiple="multiple" optionKey="id" size="5" value="${alocacaoInstance?.bensdeconsumo*.id}" class="many-to-many"/>

</div>

<div class="fieldcontain ${hasErrors(bean: alocacaoInstance, field: 'data', 'error')} required">
	<label for="data">
		<g:message code="alocacao.data.label" default="Data" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="data" precision="day"  value="${alocacaoInstance?.data}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: alocacaoInstance, field: 'permanente', 'error')} ">
	<label for="permanente">
		<g:message code="alocacao.permanente.label" default="Permanente" />
		
	</label>
	<g:select name="permanente" from="${almoxarifado.Permanente.list()}" multiple="multiple" optionKey="id" size="5" value="${alocacaoInstance?.permanente*.id}" class="many-to-many"/>

</div>

