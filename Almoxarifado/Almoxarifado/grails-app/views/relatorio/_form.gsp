<%@ page import="almoxarifado.Relatorio" %>



<div class="fieldcontain ${hasErrors(bean: relatorioInstance, field: 'registrobc', 'error')} required">
	<label for="registrobc">
		<g:message code="relatorio.registrobc.label" default="Registrobc" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="registrobc" name="registrobc.id" from="${almoxarifado.RegistroBC.list()}" optionKey="id" required="" value="${relatorioInstance?.registrobc?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: relatorioInstance, field: 'registrobp', 'error')} required">
	<label for="registrobp">
		<g:message code="relatorio.registrobp.label" default="Registrobp" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="registrobp" name="registrobp.id" from="${almoxarifado.RegistroBP.list()}" optionKey="id" required="" value="${relatorioInstance?.registrobp?.id}" class="many-to-one"/>

</div>

