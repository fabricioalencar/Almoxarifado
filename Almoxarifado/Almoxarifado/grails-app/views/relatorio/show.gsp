
<%@ page import="almoxarifado.Relatorio" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'relatorio.label', default: 'Relatorio')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-relatorio" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-relatorio" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list relatorio">
			
				<g:if test="${relatorioInstance?.registrobc}">
				<li class="fieldcontain">
					<span id="registrobc-label" class="property-label"><g:message code="relatorio.registrobc.label" default="Registrobc" /></span>
					
						<span class="property-value" aria-labelledby="registrobc-label"><g:link controller="registroBC" action="show" id="${relatorioInstance?.registrobc?.id}">${relatorioInstance?.registrobc?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${relatorioInstance?.registrobp}">
				<li class="fieldcontain">
					<span id="registrobp-label" class="property-label"><g:message code="relatorio.registrobp.label" default="Registrobp" /></span>
					
						<span class="property-value" aria-labelledby="registrobp-label"><g:link controller="registroBP" action="show" id="${relatorioInstance?.registrobp?.id}">${relatorioInstance?.registrobp?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:relatorioInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${relatorioInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
