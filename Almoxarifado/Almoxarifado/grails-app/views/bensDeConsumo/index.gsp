
<%@ page import="almoxarifado.BensDeConsumo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'bensDeConsumo.label', default: 'BensDeConsumo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-bensDeConsumo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-bensDeConsumo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="descricao" title="${message(code: 'bensDeConsumo.descricao.label', default: 'Descricao')}" />
					
						<g:sortableColumn property="quantidade" title="${message(code: 'bensDeConsumo.quantidade.label', default: 'Quantidade')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${bensDeConsumoInstanceList}" status="i" var="bensDeConsumoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${bensDeConsumoInstance.id}">${fieldValue(bean: bensDeConsumoInstance, field: "descricao")}</g:link></td>
					
						<td>${fieldValue(bean: bensDeConsumoInstance, field: "quantidade")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${bensDeConsumoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
