<%@ page import="almoxarifado.BensDeConsumo" %>



<div class="fieldcontain ${hasErrors(bean: bensDeConsumoInstance, field: 'descricao', 'error')} required">
	<label for="descricao">
		<g:message code="bensDeConsumo.descricao.label" default="Descricao" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="descricao" required="" value="${bensDeConsumoInstance?.descricao}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: bensDeConsumoInstance, field: 'quantidade', 'error')} required">
	<label for="quantidade">
		<g:message code="bensDeConsumo.quantidade.label" default="Quantidade" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="quantidade" type="number" value="${bensDeConsumoInstance.quantidade}" required=""/>

</div>

