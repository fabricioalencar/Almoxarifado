package almoxarifado

class Locador {
    
    String nome
    String rua
    String numero
    String bairro
    String sexo
    String email
    String telefone
    
    static constraints = {
        nome nullabel:false, blank: false, maxSize:35
        numero nullabel:false, blank: false, maxSize:35
        email email:true, unique:true
        sexo(inList:["Feminino", "Masculino"])
    }

       String toString(){
        nome
    }
}
