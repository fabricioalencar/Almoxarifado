package almoxarifado

class Funcionario extends Usuario{
    
    String nome
    String telefone
    String tipo
    String email
    
    static constraints = {
        tipo (inList:["Técnico", "Professor", "Estagiário", "Coordenação"])
        nome nome:true, unique:true
        email email:true, unique:false
    }
    
    String toString(){
        nome
    }
}
