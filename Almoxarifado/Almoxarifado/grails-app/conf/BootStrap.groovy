import almoxarifado.*

class BootStrap {
    def init = { servletContext ->
        //criacao do usuario em tempo de execucao
        def adminUsuario = Usuario.findByUsername('admin') ?: new Usuario(
            username: 'admin',
            password: 'admin',
            enabled: true
        ).save(failOnError: true)
        
        def regraAdmin = Regra.findByAuthority('REGRA_ADMIN') ?: new Regra(authority:'REGRA_ADMIN').save(failOnError:true)
        
        def funcionarioRegra = Regra.findByAuthority('REGRA_FUNCIONARIO') ?: new Regra(authority:'REGRA_FUNCIONARIO').save(failOnError:true)
        
        if (!adminUsuario.authorities.contains(regraAdmin)){
            UsuarioRegra.create(adminUsuario,regraAdmin, funcionarioRegra)
        }
    }
    def destroy = {
    }
    
}
