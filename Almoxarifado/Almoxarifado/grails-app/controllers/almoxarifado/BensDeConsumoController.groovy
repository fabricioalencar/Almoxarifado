package almoxarifado



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')


@Transactional(readOnly = true)
class BensDeConsumoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond BensDeConsumo.list(params), model:[bensDeConsumoInstanceCount: BensDeConsumo.count()]
    }

    def show(BensDeConsumo bensDeConsumoInstance) {
        respond bensDeConsumoInstance
    }

    def create() {
        respond new BensDeConsumo(params)
    }

    @Transactional
    def save(BensDeConsumo bensDeConsumoInstance) {
        if (bensDeConsumoInstance == null) {
            notFound()
            return
        }

        if (bensDeConsumoInstance.hasErrors()) {
            respond bensDeConsumoInstance.errors, view:'create'
            return
        }

        bensDeConsumoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bensDeConsumo.label', default: 'BensDeConsumo'), bensDeConsumoInstance.id])
                redirect bensDeConsumoInstance
            }
            '*' { respond bensDeConsumoInstance, [status: CREATED] }
        }
    }

    def edit(BensDeConsumo bensDeConsumoInstance) {
        respond bensDeConsumoInstance
    }

    @Transactional
    def update(BensDeConsumo bensDeConsumoInstance) {
        if (bensDeConsumoInstance == null) {
            notFound()
            return
        }

        if (bensDeConsumoInstance.hasErrors()) {
            respond bensDeConsumoInstance.errors, view:'edit'
            return
        }

        bensDeConsumoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'BensDeConsumo.label', default: 'BensDeConsumo'), bensDeConsumoInstance.id])
                redirect bensDeConsumoInstance
            }
            '*'{ respond bensDeConsumoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(BensDeConsumo bensDeConsumoInstance) {

        if (bensDeConsumoInstance == null) {
            notFound()
            return
        }

        bensDeConsumoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BensDeConsumo.label', default: 'BensDeConsumo'), bensDeConsumoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bensDeConsumo.label', default: 'BensDeConsumo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
