package almoxarifado



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class RegistroBPController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond RegistroBP.list(params), model:[registroBPInstanceCount: RegistroBP.count()]
    }

    def show(RegistroBP registroBPInstance) {
        respond registroBPInstance
    }

    def create() {
        respond new RegistroBP(params)
    }

    @Transactional
    def save(RegistroBP registroBPInstance) {
        if (registroBPInstance == null) {
            notFound()
            return
        }

        if (registroBPInstance.hasErrors()) {
            respond registroBPInstance.errors, view:'create'
            return
        }

        registroBPInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'registroBP.label', default: 'RegistroBP'), registroBPInstance.id])
                redirect registroBPInstance
            }
            '*' { respond registroBPInstance, [status: CREATED] }
        }
    }

    def edit(RegistroBP registroBPInstance) {
        respond registroBPInstance
    }

    @Transactional
    def update(RegistroBP registroBPInstance) {
        if (registroBPInstance == null) {
            notFound()
            return
        }

        if (registroBPInstance.hasErrors()) {
            respond registroBPInstance.errors, view:'edit'
            return
        }

        registroBPInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'RegistroBP.label', default: 'RegistroBP'), registroBPInstance.id])
                redirect registroBPInstance
            }
            '*'{ respond registroBPInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(RegistroBP registroBPInstance) {

        if (registroBPInstance == null) {
            notFound()
            return
        }

        registroBPInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'RegistroBP.label', default: 'RegistroBP'), registroBPInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'registroBP.label', default: 'RegistroBP'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
