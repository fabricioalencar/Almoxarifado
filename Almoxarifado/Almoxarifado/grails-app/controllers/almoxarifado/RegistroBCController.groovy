package almoxarifado



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')


@Transactional(readOnly = true)
class RegistroBCController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond RegistroBC.list(params), model:[registroBCInstanceCount: RegistroBC.count()]
    }

    def show(RegistroBC registroBCInstance) {
        respond registroBCInstance
    }

    def create() {
        respond new RegistroBC(params)
    }

    @Transactional
    def save(RegistroBC registroBCInstance) {
        if (registroBCInstance == null) {
            notFound()
            return
        }

        if (registroBCInstance.hasErrors()) {
            respond registroBCInstance.errors, view:'create'
            return
        }

        registroBCInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'registroBC.label', default: 'RegistroBC'), registroBCInstance.id])
                redirect registroBCInstance
            }
            '*' { respond registroBCInstance, [status: CREATED] }
        }
    }

    def edit(RegistroBC registroBCInstance) {
        respond registroBCInstance
    }

    @Transactional
    def update(RegistroBC registroBCInstance) {
        if (registroBCInstance == null) {
            notFound()
            return
        }

        if (registroBCInstance.hasErrors()) {
            respond registroBCInstance.errors, view:'edit'
            return
        }

        registroBCInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'RegistroBC.label', default: 'RegistroBC'), registroBCInstance.id])
                redirect registroBCInstance
            }
            '*'{ respond registroBCInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(RegistroBC registroBCInstance) {

        if (registroBCInstance == null) {
            notFound()
            return
        }

        registroBCInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'RegistroBC.label', default: 'RegistroBC'), registroBCInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'registroBC.label', default: 'RegistroBC'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
