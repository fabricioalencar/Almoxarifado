package almoxarifado



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class PermanenteController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Permanente.list(params), model:[permanenteInstanceCount: Permanente.count()]
    }

    def show(Permanente permanenteInstance) {
        respond permanenteInstance
    }

    def create() {
        respond new Permanente(params)
    }

    @Transactional
    def save(Permanente permanenteInstance) {
        if (permanenteInstance == null) {
            notFound()
            return
        }

        if (permanenteInstance.hasErrors()) {
            respond permanenteInstance.errors, view:'create'
            return
        }

        permanenteInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'permanente.label', default: 'Permanente'), permanenteInstance.id])
                redirect permanenteInstance
            }
            '*' { respond permanenteInstance, [status: CREATED] }
        }
    }

    def edit(Permanente permanenteInstance) {
        respond permanenteInstance
    }

    @Transactional
    def update(Permanente permanenteInstance) {
        if (permanenteInstance == null) {
            notFound()
            return
        }

        if (permanenteInstance.hasErrors()) {
            respond permanenteInstance.errors, view:'edit'
            return
        }

        permanenteInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Permanente.label', default: 'Permanente'), permanenteInstance.id])
                redirect permanenteInstance
            }
            '*'{ respond permanenteInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Permanente permanenteInstance) {

        if (permanenteInstance == null) {
            notFound()
            return
        }

        permanenteInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Permanente.label', default: 'Permanente'), permanenteInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'permanente.label', default: 'Permanente'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
