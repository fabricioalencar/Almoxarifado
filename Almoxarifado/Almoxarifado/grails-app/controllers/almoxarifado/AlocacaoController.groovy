package almoxarifado



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import org.springframework.security.access.annotation.Secured
@Secured('isFullyAuthenticated()')

@Transactional(readOnly = true)
class AlocacaoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Alocacao.list(params), model:[alocacaoInstanceCount: Alocacao.count()]
    }

    def show(Alocacao alocacaoInstance) {
        respond alocacaoInstance
    }

    def create() {
        respond new Alocacao(params)
    }

    @Transactional
    def save(Alocacao alocacaoInstance) {
        if (alocacaoInstance == null) {
            notFound()
            return
        }

        if (alocacaoInstance.hasErrors()) {
            respond alocacaoInstance.errors, view:'create'
            return
        }

        alocacaoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'alocacao.label', default: 'Alocacao'), alocacaoInstance.id])
                redirect alocacaoInstance
            }
            '*' { respond alocacaoInstance, [status: CREATED] }
        }
    }

    def edit(Alocacao alocacaoInstance) {
        respond alocacaoInstance
    }

    @Transactional
    def update(Alocacao alocacaoInstance) {
        if (alocacaoInstance == null) {
            notFound()
            return
        }

        if (alocacaoInstance.hasErrors()) {
            respond alocacaoInstance.errors, view:'edit'
            return
        }

        alocacaoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Alocacao.label', default: 'Alocacao'), alocacaoInstance.id])
                redirect alocacaoInstance
            }
            '*'{ respond alocacaoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Alocacao alocacaoInstance) {

        if (alocacaoInstance == null) {
            notFound()
            return
        }

        alocacaoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Alocacao.label', default: 'Alocacao'), alocacaoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'alocacao.label', default: 'Alocacao'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
